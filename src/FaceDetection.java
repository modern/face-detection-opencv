import com.googlecode.javacv.cpp.opencv_core.IplImage;
import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_imgproc.*;
import static com.googlecode.javacv.cpp.opencv_highgui.*;
import static com.googlecode.javacv.cpp.opencv_objdetect.*;

public class FaceDetection {
	private static final int SCALE = 2;
    // ����������� ��������������� ��� ���������� ������� ��������� �����������
    public static void main(String[] args){
    	// ������ ��� ����������� ����
    	String CASCADE_FILE ="haarcascade_frontalface_alt2.xml";
        String OUT_FILE = "found_face.jpg";
        //�������� �����������
        IplImage origImg = cvLoadImage("nirvana.jpg", 1);
        // �������������� � ������� ������
        IplImage grayImg = IplImage.create(origImg.width(),origImg.height(), IPL_DEPTH_8U, 1);
        cvCvtColor(origImg, grayImg, CV_BGR2GRAY);  
        // ���������������� � �������� ������ (��� ��������� ����������� ����)
        IplImage smallImg = IplImage.create(grayImg.width()/SCALE,grayImg.height()/SCALE, IPL_DEPTH_8U, 1);
            cvResize(grayImg, smallImg, CV_INTER_LINEAR);
        // ������������ �������� ������
        IplImage equImg = IplImage.create(smallImg.width(),smallImg.height(), IPL_DEPTH_8U, 1);
        cvEqualizeHist(smallImg, equImg);
         // �������� ���������� ���������, ������������� ��� ����������� �������
         CvMemStorage storage = CvMemStorage.create();
         // ������������� ������� ��� ����������� ����
         CvHaarClassifierCascade cascade = new CvHaarClassifierCascade(cvLoad(CASCADE_FILE));
         System.out.println("����� ���...");
         CvSeq faces = cvHaarDetectObjects(equImg, cascade, storage,1.1, 3, CV_HAAR_DO_CANNY_PRUNING);
         cvClearMemStorage(storage);
         // ���������� ������������� ������ ���� ���
         int total = faces.total();
         System.out.println("������� " + total + " ���(�)");

         for (int i = 0; i < total; i++) {
        	 CvRect r = new CvRect(cvGetSeqElem(faces, i));
        	 cvRectangle(origImg, cvPoint( r.x()*SCALE, r.y()*SCALE ),cvPoint( (r.x() + r.width())*SCALE,(r.y() + r.height())*SCALE ),CvScalar.RED, 3, CV_AA, 0);
        	 String strRect = String.format("CvRect(%d,%d,%d,%d)", r.x(), r.y(), r.width(), r.height());
             System.out.println(strRect);
             //�������� ��������������� ����������� ��� ������� ��������� ��������������
            }
         
         if (total > 0) {
        	 System.out.println("���������� �����������" + " ��� " + OUT_FILE);
        	 cvSaveImage(OUT_FILE, origImg);
            }       
    }
}